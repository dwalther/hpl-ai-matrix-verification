function rel_err = hpl_sensibility(n, kappa, eps)
%HPL_SENSIBILITY   Test the sensibility of HPL-AI matrix.
%   REL_ERR = HPL_SENSIBILITY(N,KAPPA,EPS) computes the relative error in
%   solving a system of linear equations Ax = b, where A is an HPL-AI matrix
%   of size N and condition number KAPPA, and b is a randomly generated
%   vector. The function tests how a small perturbation in the vector x
%   affects the solution, by computing A*(x + dx)=b_pert and cehcking the
%   relative error between b and b_pert where dx is a small perturbation vector 
%   with norm ||dx||_inf = EPS.
%
% Inputs:
% - n: Positive integer scalar specifying the dimension of the matrix.
% - kappa: Positive scalar specifying the desired condition number of the matrix.
% - eps : Positive scalar specifying the scale of the perturbation
% 
% Output:
% - rel_err: relative error between b and b_pert = A*(x+dx).

    % Generate HPL-AI matrix A
    A = hpl_ai_matrix(n, kappa);

    % Generate random vector b
    b = randn(n, 1);

    % Solve system Ax = b
    x = A \ b;

    % Generate perturbation vector dx
    dx = eps * randn(n, 1);

    % Solve system A(x+dx) = b
    b_pert = A * (x + dx);

    % Compute relative error
    rel_err = norm(b - b_pert, inf) / norm(b, inf);
end
