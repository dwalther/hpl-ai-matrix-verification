% Generate matrices with hpl_ai_matrix and randsvd
n = [100, 1000, 10000];
kappa = 1000;

hpl_ai_matrices = cell(1, length(n));
randsvd_matrices = cell(1, length(n));

% Measure computation time and condition number for each matrix
hpl_ai_times = zeros(1, length(n));
hpl_ai_cond = zeros(1, length(n));
randsvd_times = zeros(1, length(n));
randsvd_cond = zeros(1, length(n));
for i = 1:length(n)
    % Measure computation time for hpl_ai_matrix
    tic;
    hpl_ai_matrices{i} = hpl_ai_matrix(n(i), kappa);
    hpl_ai_times(i) = toc;
    
    % Measure condition number for hpl_ai_matrix
    hpl_ai_cond(i) = cond(hpl_ai_matrices{i}, inf);
    
    % Measure computation time for randsvd
    tic;
    randsvd_matrices{i} = gallery('randsvd', n(i),kappa);
    randsvd_times(i) = toc;
    
    % Measure condition number for randsvd
    randsvd_cond(i) = cond(randsvd_matrices{i}, inf);
end

% Generate table in LaTeX format
fprintf('\\begin{table}[ht]\n');
fprintf('\\caption{Comparison of performance and properties of matrices generated with \\texttt{hpl\\_ai\\_matrix} and \\texttt{randsvd}.}\n');
fprintf('\\centering\n');
fprintf('\\begin{tabular}{c c c c c c c}\n');
fprintf('\\hline\n');
fprintf('Size & $\\kappa$ & $\\|A\\|_{\\infty}$ & HPL-AI time (s) & HPL-AI cond. & RandSVD time (s) & RandSVD cond. \\\\\n');
fprintf('\\hline\n');
for i = 1:length(n)
    fprintf('%d & %d & %.2f & %.2f & %.2e & %.2f & %.2e \\\\\n', n(i), kappa, norm(hpl_ai_matrices{i}, inf), hpl_ai_times(i), hpl_ai_cond(i), randsvd_times(i), randsvd_cond(i));
end
fprintf('\\hline\n');
fprintf('\\end{tabular}\n');
fprintf('\\end{table}\n');